# Unity JS Addon
  - Camera
  - Gyroscope
  - Accelerometer

Add ***manifest.json*** and ***addon.js*** to head tag in *index.html*
```html
<link rel="manifest" href="./manifest.json">
<script type="text/javascript" src="./addon.js">
```
# Camera
#### Init camera object
```js
var camera = new Camera();
```
#### Get device list
```js
camera.getDevicesList().then(devices => {
    console.log(devices)
    /* devices is array of MediaDeviceInfo objects
        [{
            deviceId: "61272d9cd417ea94feb"
            groupId: ""
            kind: "1videoinput"
            label: "USB2.0 UVC VGA WebCam↵ (13d3:5710)"
        },
        ... ]
    */
})
```

#### Get Stream by deviceId
```js
camera.getStream("61272d9cd417ea94feb").then(function(stream) {
    var video = document.querySelector('video');
    video.srcObject = stream;
    video.onloadedmetadata = function() {
        video.play();
    };
}).catch(e => {
    alert(e.message)
})
```

# Gyroscope
#### Init gyroscope object
```js
var gyro = new Gyroscope();
```
#### Listen event
```js
window.addEventListener('gyroscope', e => {
    console.log('gyroscope', e.detail)
    // will show { a: 0, b: 0, g: 0}
})
```
#### Get last gyroscope data
```js
gyro.get()
// will return { a: 0, b: 0, g: 0}
```
#### Start/stop event dispatching
```js
gyro.startPropagation() // start dispatching `gyroscope` event
gyro.stopPropagation() // stop dispatching `gyroscope` event
```

# Accelerometer
#### Init accelerometer object
```js
var accel = new Accelerometer();
```
#### Listen event
```js
window.addEventListener('accelerometer', e => {
    console.log('accelerometer', e.detail)
    // will show { x: 0, y: 0, z: 0}
})
```
#### Get last accelerometer data
```js
accel.get()
// will return { x: 0, y: 0, z: 0}
```
#### Start/stop event dispatching
```js
accel.startPropagation() // start dispatching `accelerometer` event
accel.stopPropagation() // stop dispatching `accelerometer` event
```

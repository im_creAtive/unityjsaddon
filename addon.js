function Gyroscope() {
  this.data = {a: 0, b: 0, g: 0};
  this.castEvent = true;

  // start event dispatching
  this.startPropagation = function() {
    this.castEvent = true;
  }

  // stop event dispatching
  this.stopPropagation = function() {
    this.castEvent = false;
  }

  // get last ABG of Gyroscope
  this.get = function(){
    return this.data;
  }

  this.handleOrientationEvent = function(e){
    this.data = {
      a: e.alpha !== null ? e.aplpha : 0,
      b: e.beta !== null ? e.beta : 0,
      g: e.gamma !== null ? e.gamma : 0
    }

    if(this.castEvent) window.dispatchEvent(this.event);
  }

  // custom event
  this.event = new CustomEvent('gyroscope', { 'detail': this.data });
  // std event
  window.addEventListener('deviceorientation', this.handleOrientationEvent.bind(this), false);
}

function Accelerometer() {
  this.data = {x: 0, y: 0, z: 0};
  this.castEvent = true;

  // start event dispatching
  this.startPropagation = function() {
    this.castEvent = true;
  }

  // stop event dispatching
  this.stopPropagation = function() {
    this.castEvent = false;
  }

  // get last XYZ of Accelerometer
  this.get = function(){
    return this.data;
  }

  this.handleDeviceMotionEvent = function(e){
    this.data = {
      x: e.accelerationIncludingGravity.x !== null ? e.accelerationIncludingGravity.x : 0,
      y: e.accelerationIncludingGravity.y !== null ? e.accelerationIncludingGravity.y : 0,
      z: e.accelerationIncludingGravity.z !== null ? e.accelerationIncludingGravity.z : 0
    }

    if(this.castEvent) window.dispatchEvent(this.event);
  }

  // custom event
  this.event = new CustomEvent('accelerometer', { 'detail': this.data });
  // std event
  window.addEventListener('devicemotion', this.handleDeviceMotionEvent.bind(this), false);
}

function Camera(){
  var camera = {
    getDevicesList: function(){
      return new Promise((resolve, reject) => {
        navigator.getUserMedia ({
            video: true
         }, () => {
           if (!navigator.mediaDevices || !navigator.mediaDevices.enumerateDevices) {
             throw new Error("enumerateDevices not supported.");
           }

           var devices = [];
           navigator.mediaDevices.enumerateDevices().then(function(devices) {
              devices.forEach(function(device) {
                devices.push(device);
              });

              resolve(devices);
           }).catch(function(err) {
              throw new Error(err.message);
           });
         }, e => {
          throw new Error(e.message);
         });
      })
    },
    getStream: function(deviceId){
      var constraints = { deviceId: { exact: deviceId } };
      return navigator.mediaDevices.getUserMedia({ video: constraints });
    }
  }

  return camera;
}
